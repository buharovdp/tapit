import { configureStore } from '@reduxjs/toolkit';
import tokenSlice from 'redux/token.js';
import userSlice from 'redux/user.js';

export default configureStore({
    reducer: {
        token: tokenSlice,
        user: userSlice
    },
})