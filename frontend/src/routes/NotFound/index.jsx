import AppContainer from "components/AppContainer";
import AppBody from "components/AppBody";
import Center from "components/CenterWrapper";
import NavBar from "components/NavBar";
import Footer from "components/Footer";

import "./index.scoped.css";

function Sign() {
    return (
        <AppContainer>
            <AppBody>
                <NavBar signOut={false} headerText={"Slovenly Polygon"} />
            </AppBody>
            <Center>
                <AppBody>
                    <div className="not-found">
                        <div className="not-found-img">
                            <img src={require('./404.png')} alt="" />
                        </div>
                        <div className="text-wrapper">
                            <h1>Oops!</h1>
                            <div className="error">PAGE NOT FOUND</div>
                            <div className="message">The page you are looking for does not exist.</div>
                            <a className="button" href="/">Backward</a>
                        </div>
                    </div>
                </AppBody>
            </Center>
            <Footer />
        </AppContainer>
    );
}

export default Sign;