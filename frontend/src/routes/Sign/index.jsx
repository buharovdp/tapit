import {useState, useRef, useEffect} from 'react';
import {useNavigate} from 'react-router-dom';

import 'primereact/resources/primereact.min.css'
import 'primeicons/primeicons.css'

import { Toast } from 'primereact/toast';


import AppContainer from "components/AppContainer";
import AppBody from "components/AppBody";
import Center from "components/CenterWrapper";
import NavBar from "components/NavBar";
import Footer from "components/Footer";

import {useSelector, useDispatch} from 'react-redux';
import {selectToken, setToken} from "redux/token.js";
import {selectUser, setUser} from "redux/user.js";

import "./index.scoped.css";

function Sign() {
    const [active, setActive] = useState(false);
    const toast = useRef();

    const LOGIN_ERROR = "Login can't be empty";
    const PWD_ERROR = "Password can't be empty";
    const PWD_MATCH_ERROR = "Passwords don't match";

    const SIGN_URL = "/dima/lab-4/api/sign";

    // For sign in form
    const [username, setUsername] = useState("")
    const [password, setPassword] = useState("")

    // For sign up form
    const [newUsername, setNewUsername] = useState("")
    const [newPassword, setNewPassword] = useState("")
    const [newPasswordAgain, setNewPasswordAgain] = useState("")

    const dispatch = useDispatch()
    const navigate = useNavigate();
    const token = useSelector(selectToken);
    const user = useSelector(selectUser);

    useEffect(() => {
        checkAuth();
    }, [])

    function signUpRequest(event) {
        event.preventDefault();

        if (newUsername === null || newUsername === "") {
            toast.current.show({ sticky: false, life: 2000, closable: true, severity: "error", summary: "Error", detail: LOGIN_ERROR});
            return;
        }

        if (newPassword === null || newPassword === "") {
            toast.current.show({ sticky: false, life: 2000, closable: true, severity: "error", summary: "Error", detail: PWD_ERROR});
            return;
        }

        if (newPassword !== newPasswordAgain) {
            toast.current.show({ sticky: false, life: 2000, closable: true, severity: "error", summary: "Error", detail: PWD_MATCH_ERROR});
            return;
        }

        (async () => {
            let data = new FormData();
            data.append("username", newUsername);
            data.append("password", newPassword);
            let res = await fetch(SIGN_URL + "/up", { method: "POST", body: data });
            let message = await res.json();
            if (message.hasOwnProperty("message")){
                toast.current.show({ sticky: false, life: 2000, closable: true, severity: "error", summary: "Success", detail: message["message"]});
                return;
            }
            toast.current.show({ sticky: false, life: 2000, closable: true, severity: "info", summary: "Success", detail: "Account was created"});
            dispatch(setToken(message["jwt-token"]));
            dispatch(setUser(newUsername));
            await new Promise(resolve => setTimeout(resolve, 1000));
            navigate("/app", {replace: true});
        })()
    }

    function signInRequest(event) {
        event.preventDefault();

        if (username === null || username === "") {
            toast.current.show({ sticky: false, life: 2000, closable: true, severity: "error", summary: "Error", detail: LOGIN_ERROR});
            return;
        }

        if (password === null || password === "") {
            toast.current.show({ sticky: false, life: 2000, closable: true, severity: "error", summary: "Error", detail: PWD_ERROR});
            return;
        }

        (async () => {
            let data = new FormData();
            data.append("username", username);
            data.append("password", password);
            let res = await fetch(SIGN_URL + "/in", { method: "POST", body: data });
            let message = await res.json();
            if (message.hasOwnProperty("message")){
                toast.current.show({ sticky: false, life: 2000, closable: true, severity: "error", summary: "Success", detail: message["message"]});
                return;
            }
            toast.current.show({ sticky: false, life: 2000, closable: true, severity: "info", summary: "Success", detail: "Successful sign-in"});
            dispatch(setToken(message["jwt-token"]));
            dispatch(setUser(username));
            await new Promise(resolve => setTimeout(resolve, 1000));
            navigate("/app", {replace: true});
        })()
    }

    function checkAuth() {
        (async () => {
            let res = await fetch("/dima/lab-4/api/points", {method: "GET", headers: {"Authorization": token}});
            let message = await res.json();
            if (res.ok && !message.hasOwnProperty("message")) {
                navigate("/app", {replace: true});
            }
        })()
    }

    return (
        <AppContainer>
            <AppBody>
                <NavBar signOut={false} headerText={"Slovenly Polygon"}/>
            </AppBody>
            <Center>
                <AppBody>
                    <div className="sign-wrapper">
                        <div className={"sign-container " + (active ? "right-panel-active" : "")} id="sign-container">
                            <div className="form-container sign-up-container">
                                <form action="#" onSubmit={signUpRequest}>
                                    <h1>Create Account</h1>
                                    <span>Come up with your login and password</span>
                                    <input id="sign-up-login" type="text" placeholder="Login" onChange={e => setNewUsername(e.target.value)}/>
                                    <input id="sign-up-pwd" type="password" placeholder="Password" onChange={e => setNewPassword(e.target.value)}/>
                                    <input id="sign-up-pwd-again" type="password" placeholder="Repeat password" onChange={e => setNewPasswordAgain(e.target.value)}/>
                                    <button>Sign Up</button>
                                </form>
                            </div>
                            <div className="form-container sign-in-container">
                                <form action="#" onSubmit={signInRequest}>
                                    <h1>Sign in</h1>
                                    <span>Input your login and password</span>
                                    <input id="sign-in-login" type="login" placeholder="Login" onChange={e => setUsername(e.target.value)}/>
                                    <input id="sign-in-pwd" type="password" placeholder="Password" onChange={e => setPassword(e.target.value)}/>
                                    <button>Sign In</button>
                                </form>
                            </div>
                            <div className="overlay-container">
                                <div className="overlay">
                                    <div className="overlay-panel overlay-left">
                                        <div className="img-wrapper">
                                            <img src={require('./pic1.png')} alt=""/>
                                        </div>
                                        <h1>Welcome Back!</h1>
                                        <p>To keep connected with me please login with your personal info</p>
                                        <button className="ghost" id="signIn" onClick={() => { setActive(false) }}>Sign In</button>
                                    </div>
                                    <div className="overlay-panel overlay-right">
                                        <div className="img-wrapper">
                                            <img src={require('./pic2.png')} alt=""/>
                                        </div>
                                        <h1>Hello, Friend!</h1>
                                        <p>Enter your personal details and start journey with me</p>
                                        <button className="ghost" id="signUp" onClick={() => { setActive(true) }}>Sign Up</button>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </AppBody>
            </Center>
            <div className="toast-block">
                <Toast ref={toast} position="bottom-right" />
            </div>
            <Footer/>
        </AppContainer>
    );
}

export default Sign;