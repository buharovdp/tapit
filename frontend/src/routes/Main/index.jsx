import {useRef, useState, useEffect} from 'react';
import {useNavigate} from 'react-router-dom';

import 'primereact/resources/primereact.min.css'
import 'primeicons/primeicons.css'

import {Toast} from 'primereact/toast';
import {Checkbox} from 'primereact/checkbox';
import {Slider} from 'primereact/slider';

import {useSelector} from 'react-redux';
import {selectToken} from 'redux/token.js';
import {selectUser} from 'redux/user.js';

import AppContainer from "components/AppContainer";
import AppBody from "components/AppBody";
import Center from "components/CenterWrapper";
import NavBar from "components/NavBar";
import Footer from "components/Footer";

import "./theme.css";
import "./index.scoped.css";
import "./form.scoped.css";

function Main() {
    const token = useSelector(selectToken);
    const user = useSelector(selectUser);
    const navigate = useNavigate();

    const [active, setActive] = useState(false);
    const [xValue, setNewX] = useState("0");
    const [yValue, setNewY] = useState(0);
    const [rValue, setNewR] = useState("1");
    const [data, setData] = useState([]);
    const toast = useRef();
    const pointsCanvasRef = useRef();

    const RADIUS_ERROR = "Radius can't be less than 1";

    let xForCanvasFix = getX();
    let yForCanvasFix = getY();
    let rForCanvasFix = getR();

    useEffect(() => {
        checkAuth();
        getPointsRequest();
    }, []);

    useEffect(() => {
        drawUserPoints();
    }, [data, pointsCanvasRef, rValue])

    const selectX = (e) => {
        setX(e.value + "");
        xForCanvasFix = e.value;
        updateCanvas();
    }

    const selectY = (e) => {
        setY(e.value);
        yForCanvasFix = e.value;
        updateCanvas();
    }

    const selectR = (e) => {
        if (e.value <= 0) {
            const errorCheckbox = document.getElementById(e.target.id);
            errorCheckbox.parentNode.classList.add('error-r');
            toast.current.show({
                sticky: false,
                life: 2000,
                closable: true,
                severity: "error",
                summary: "Error",
                detail: RADIUS_ERROR
            });
        } else {
            setR(e.value);
            rForCanvasFix = e.value;
            // drawUserPoints();
            updateCanvas();
        }
    }

    const CANVAS_OFFSET_X = 170;
    const CANVAS_OFFSET_Y = 170;
    const CANVAS_SCALE_X = 104;
    const CANVAS_SCALE_Y = 104;

    const style = getComputedStyle(document.body);

    function getX() {
        return xValue;
    }

    function getY() {
        return yValue;
    }

    function getR() {
        return rValue;
    }

    function setX(xValue) {
        setNewX(xValue + "");
    }

    function setY(yValue) {
        setNewY(yValue);
    }

    function setR(rValue) {
        setNewR(rValue + "");
    }

    function validateX() {
        return xValue >= -3 && xValue <= 5;
    }

    function validateY() {
        return yValue >= -3 && yValue <= 5;
    }

    function validateR() {
        return rValue >= 1 && rValue <= 5;
    }

    function updateCanvas() {
        let canvas = document.getElementById("canvas");
        let context = canvas.getContext("2d");
        context.clearRect(0, 0, canvas.width, canvas.height);
        if (validateX() && validateY() && validateR()) drawPossiblePoint(xForCanvasFix, yForCanvasFix, rForCanvasFix, context);
    }

    let lastX = getX();
    let lastY = getY();
    let r = getR();

    function drawPossiblePoint(x, y, r, context) {
        x = x * CANVAS_SCALE_X / r + CANVAS_OFFSET_X;
        y = -y * CANVAS_SCALE_Y / r + CANVAS_OFFSET_Y;

        context.lineWidth = 2;
        context.strokeStyle = "#fff";
        context.beginPath();
        context.moveTo(x - 4, y - 4);
        context.lineTo(x + 4, y + 4);
        context.moveTo(x + 4, y - 4);
        context.lineTo(x - 4, y + 4);
        context.stroke();
    }

    function drawCursor(event) {
        let canvas = event.target;
        let context = canvas.getContext("2d");

        let rect = canvas.getBoundingClientRect();
        let sx = event.clientX - rect.left;
        let sy = event.clientY - rect.top;

        context.clearRect(0, 0, canvas.width, canvas.height);
        context.lineWidth = 2;
        if (validateR()) context.strokeStyle = "#fff";
        else context.strokeStyle = style.getPropertyValue('--errorcolor');
        context.beginPath();
        context.arc(sx, sy, 4, 0, 2 * Math.PI);
        context.stroke();

        if (validateR()) r = getR();

        let x = sx - CANVAS_OFFSET_X;
        let y = sy - CANVAS_OFFSET_Y;
        x = Math.round(x / CANVAS_SCALE_X * r);
        y = Math.round(-y / CANVAS_SCALE_Y * r);

        if (x < -3) x = -3;
        if (x > 5) x = 5;
        if (x === -0) x = 0;
        if (y <= -3) y = -3;
        if (y >= 5) y = 5;
        if (y === -0) y = 0;

        lastX = x;
        lastY = y;

        drawPossiblePoint(x, y, r, context);
    }

    function onCanvasClick(event) {
        setX(lastX);
        setY(lastY);
        setR(r);

        validateX();
        validateY();
        validateR();
    }

    function onCanvasLeave(event) {
        let canvas = event.target;
        let context = canvas.getContext("2d");
        context.clearRect(0, 0, canvas.width, canvas.height);
        drawPossiblePoint(xForCanvasFix, yForCanvasFix, rForCanvasFix, context);
    }

    function drawUserPoints() {
        let set = new Set();

        for (let p of data) {
            if (p["owner"] === user) {
                set.add(p["x"] + " " + p["y"] + " " + p["r"] + " " + p["result"]);
            }
        }

        const pointsCanvas = pointsCanvasRef.current;
        const pointsContext = pointsCanvas.getContext("2d");
        pointsContext.clearRect(0, 0, pointsCanvas.width, pointsCanvas.height);
        for (let data of set) {
            data = data.split(" ");
            let xCoord = parseInt(data[0]);
            let yCoord = parseInt(data[1]);
            let radius = parseInt(data[2]);
            let result = data[3];

            if (rValue == radius) {
                xCoord = xCoord * CANVAS_SCALE_X / radius + CANVAS_OFFSET_X;
                yCoord = -yCoord * CANVAS_SCALE_Y / radius + CANVAS_OFFSET_Y;

                pointsContext.lineWidth = 2;

                if (result !== "no") pointsContext.strokeStyle = "#efeab0";
                else pointsContext.strokeStyle = "#FF3B6C";

                pointsContext.beginPath();
                pointsContext.arc(xCoord, yCoord, 6, 0, 2 * Math.PI);

                pointsContext.moveTo(xCoord - 4, yCoord - 4);
                pointsContext.lineTo(xCoord + 4, yCoord + 4);

                pointsContext.moveTo(xCoord + 4, yCoord - 4);
                pointsContext.lineTo(xCoord - 4, yCoord + 4);
                pointsContext.stroke();
            }

        }

    }

    function addPointRequest(event) {
        event.preventDefault();

        (async () => {
            let data = new FormData();
            data.append("x", xValue);
            data.append("y", yValue.toString());
            data.append("r", rValue);
            let res = await fetch("/dima/lab-4/api/points", {method: "POST", headers: {"Authorization": token}, body: data});
            let message = await res.json();
            if (message.hasOwnProperty("message") && message["message"] === "unauthorised") {
                toast.current.show({
                    sticky: false,
                    life: 2000,
                    closable: true,
                    severity: "info",
                    summary: "Info",
                    detail: "Token was expired, sign in again please"
                });
                await new Promise(resolve => setTimeout(resolve, 1500));
                navigate("/", {replace: true});
                return;
            }
            if (message.hasOwnProperty("message") && message["message"] === "success") {
                toast.current.show({
                    sticky: false,
                    life: 2000,
                    closable: true,
                    severity: "info",
                    summary: "Success",
                    detail: "Point was successfully added"
                });
                getPointsRequest();
            }
        })()
    }

    function getPointsRequest() {
        (async () => {
            let res = await fetch("/dima/lab-4/api/points", {method: "GET", headers: {"Authorization": token}});
            let message = await res.json();
            if (message.hasOwnProperty("message") && message["message"] === "unauthorised") {
                toast.current.show({
                    sticky: false,
                    life: 2000,
                    closable: true,
                    severity: "info",
                    summary: "Info",
                    detail: "Token was expired, sign in again please"
                });
                await new Promise(resolve => setTimeout(resolve, 1500));
                navigate("/", {replace: true});
            }
            if (res.ok) {
                setData(message);
            }
        })()
    }

    function checkAuth() {
        (async () => {
            let res = await fetch("/dima/lab-4/api/points", {method: "GET", headers: {"Authorization": token}});
            let message = await res.json();
            if (message.hasOwnProperty("message") && message["message"] === "unauthorized") {
                navigate("/", {replace: true});
            }
        })()
    }

    function toLocalDate(time) {
        let m = new Date(time);
        let tz = Intl.DateTimeFormat().resolvedOptions().timeZone
        m = m.toLocaleString('ru-RU', { timeZone: tz});
        return m + " " + tz;
    }

    return (
        <AppContainer>
            <AppBody>
                <NavBar signOut={true} headerText={"Sign Out (" + user + ")"}/>
            </AppBody>
            <Center>
                <AppBody>
                    <div className="main-wrapper">
                        <div className={"main-card " + (active ? "table-active" : "")} id="main-card">
                            <div className="form-container">
                                <div className="table-anim-wrapper">
                                    <div className="form-wrapper">
                                        <div className="layout">
                                            <form onSubmit={addPointRequest}>
                                                <input type="hidden" name="x" value={xValue[0]}/>
                                                <input type="hidden" name="y" value={yValue}/>
                                                <input type="hidden" name="r" value={rValue[0]}/>
                                                <h2>Add point</h2>
                                                <div className="value-wrapper">
                                                    <div className="form-header">Choose X:</div>
                                                    <div className="input-wrapper">
                                                        <div className="check-wrapper">
                                                            <label htmlFor="x-3" className="p-checkbox-label">-3</label>
                                                            <Checkbox id="x-3" value="-3" onChange={selectX}
                                                                      checked={xValue === ('-3')}></Checkbox>
                                                        </div>
                                                        <div className="check-wrapper">
                                                            <label htmlFor="x-2" className="p-checkbox-label">-2</label>
                                                            <Checkbox id="x-2" value="-2" onChange={selectX}
                                                                      checked={xValue === ('-2')}></Checkbox>
                                                        </div>
                                                        <div className="check-wrapper">
                                                            <label htmlFor="x-1" className="p-checkbox-label">-1</label>
                                                            <Checkbox id="x-1" value="-1" onChange={selectX}
                                                                      checked={xValue === ('-1')}></Checkbox>
                                                        </div>
                                                        <div className="check-wrapper">
                                                            <label htmlFor="x0" className="p-checkbox-label">0</label>
                                                            <Checkbox id="x0" value="0" onChange={selectX}
                                                                      checked={xValue === ('0')}></Checkbox>
                                                        </div>
                                                        <div className="check-wrapper">
                                                            <label htmlFor="x1" className="p-checkbox-label">1</label>
                                                            <Checkbox id="x1" value="1" onChange={selectX}
                                                                      checked={xValue === ('1')}></Checkbox>
                                                        </div>
                                                        <div className="check-wrapper">
                                                            <label htmlFor="x2" className="p-checkbox-label">2</label>
                                                            <Checkbox id="x2" value="2" onChange={selectX}
                                                                      checked={xValue === ('2')}></Checkbox>
                                                        </div>
                                                        <div className="check-wrapper">
                                                            <label htmlFor="x3" className="p-checkbox-label">3</label>
                                                            <Checkbox id="x3" value="3" onChange={selectX}
                                                                      checked={xValue === ('3')}></Checkbox>
                                                        </div>
                                                        <div className="check-wrapper">
                                                            <label htmlFor="x4" className="p-checkbox-label">4</label>
                                                            <Checkbox id="x4" value="4" onChange={selectX}
                                                                      checked={xValue === ('4')}></Checkbox>
                                                        </div>
                                                        <div className="check-wrapper">
                                                            <label htmlFor="x5" className="p-checkbox-label">5</label>
                                                            <Checkbox id="x5" value="5" onChange={selectX}
                                                                      checked={xValue === ('5')}></Checkbox>
                                                        </div>
                                                    </div>
                                                </div>
                                                <div className="value-wrapper">
                                                    <div className="form-header">Choose Y:&nbsp;&nbsp;{yValue}</div>
                                                    <div className="input-wrapper y-wrapper">
                                                        <Slider className='slider' value={yValue} min={-3} max={5}
                                                                step={1} onChange={selectY} style={{width: '100%'}}/>
                                                    </div>
                                                </div>
                                                <div className="value-wrapper">
                                                    <div className="form-header">Choose R:</div>
                                                    <div className="input-wrapper">
                                                        <div className="check-wrapper">
                                                            <label htmlFor="r-3" className="p-checkbox-label">-3</label>
                                                            <Checkbox id="r-3" value="-3" onChange={selectR}
                                                                      checked={rValue === ('-3')}></Checkbox>
                                                        </div>
                                                        <div className="check-wrapper">
                                                            <label htmlFor="r-2" className="p-checkbox-label">-2</label>
                                                            <Checkbox id="r-2" value="-2" onChange={selectR}
                                                                      checked={rValue === ('-2')}></Checkbox>
                                                        </div>
                                                        <div className="check-wrapper">
                                                            <label htmlFor="r-1" className="p-checkbox-label">-1</label>
                                                            <Checkbox id="r-1" value="-1" onChange={selectR}
                                                                      checked={rValue === ('-1')}></Checkbox>
                                                        </div>
                                                        <div className="check-wrapper">
                                                            <label htmlFor="r0" className="p-checkbox-label">0</label>
                                                            <Checkbox id="r0" value="0" onChange={selectR}
                                                                      checked={rValue === ('0')}></Checkbox>
                                                        </div>
                                                        <div className="check-wrapper">
                                                            <label htmlFor="r1" className="p-checkbox-label">1</label>
                                                            <Checkbox id="r1" value="1" onChange={selectR}
                                                                      checked={rValue === ('1')}></Checkbox>
                                                        </div>
                                                        <div className="check-wrapper">
                                                            <label htmlFor="r2" className="p-checkbox-label">2</label>
                                                            <Checkbox id="r2" value="2" onChange={selectR}
                                                                      checked={rValue === ('2')}></Checkbox>
                                                        </div>
                                                        <div className="check-wrapper">
                                                            <label htmlFor="r3" className="p-checkbox-label">3</label>
                                                            <Checkbox id="r3" value="3" onChange={selectR}
                                                                      checked={rValue === ('3')}></Checkbox>
                                                        </div>
                                                        <div className="check-wrapper">
                                                            <label htmlFor="r4" className="p-checkbox-label">4</label>
                                                            <Checkbox id="r4" value="4" onChange={selectR}
                                                                      checked={rValue === ('4')}></Checkbox>
                                                        </div>
                                                        <div className="check-wrapper">
                                                            <label htmlFor="r5" className="p-checkbox-label">5</label>
                                                            <Checkbox id="r5" value="5" onChange={selectR}
                                                                      checked={rValue === ('5')}></Checkbox>
                                                        </div>
                                                    </div>
                                                </div>
                                                <input className="submit" type="submit" value={"Submit"}/>
                                            </form>
                                        </div>
                                    </div>
                                    <div className="table-wrapper">
                                        {data && <table className="table">
                                            <thead>
                                            <tr className="table-header">
                                                <th>X</th>
                                                <th>Y</th>
                                                <th>R</th>
                                                <th>Request time</th>
                                                <th>Result</th>
                                                <th>Owner</th>
                                            </tr>
                                            </thead>
                                            <tbody>
                                            {data.map(
                                                (point, i) => (
                                                    <tr className={(point.result === "no" ? "no" : "")} key={i}>
                                                        <td>{point.x}</td>
                                                        <td>{point.y}</td>
                                                        <td>{point.r}</td>
                                                        <td>{toLocalDate(point.requestTimestamp)}</td>
                                                        <td>{point.result}</td>
                                                        <td>{point.owner}</td>
                                                    </tr>
                                                )
                                            )}
                                            </tbody>
                                        </table>}
                                    </div>
                                </div>
                            </div>
                            <div className="canvas-container">
                                <div className="canvas-anim-wrapper">
                                    <div className="canvas-wrapper">
                                        <div className="layout">
                                            <canvas id="canvas" width={340} height={340}
                                                    onMouseMove={drawCursor} onClick={onCanvasClick}
                                                    onMouseLeave={onCanvasLeave}/>
                                            <canvas className={"r" + getR()} id="points-canvas" width={340} height={340} ref={pointsCanvasRef}/>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div className="switcher" onClick={() => {
                                setActive(!active);
                                getPointsRequest();
                            }}>
                                <svg width="16px" height="16px" viewBox="0 0 16 16" xmlns="http://www.w3.org/2000/svg"
                                     fill="currentColor" className="bi bi-arrow-bar-up">
                                    <path fillRule="evenodd"
                                          d="M8 10a.5.5 0 0 0 .5-.5V3.707l2.146 2.147a.5.5 0 0 0 .708-.708l-3-3a.5.5 0 0 0-.708 0l-3 3a.5.5 0 1 0 .708.708L7.5 3.707V9.5a.5.5 0 0 0 .5.5zm-7 2.5a.5.5 0 0 1 .5-.5h13a.5.5 0 0 1 0 1h-13a.5.5 0 0 1-.5-.5z"/>
                                </svg>
                            </div>
                        </div>
                    </div>
                </AppBody>
            </Center>
            <div className="toast-block">
                <Toast ref={toast} position="bottom-right"/>
            </div>
            <Footer/>
        </AppContainer>
    );
}

export default Main;