import "./index.scoped.css";


function Footer(props) {

    return (
        <footer>
            <p>ITMO University</p>
            <p>Dmitry Buharov P32101</p>
            <p>Saint-Petersburg 2022</p>
        </footer>
    );
}

export default Footer;