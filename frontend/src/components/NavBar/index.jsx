import "./index.scoped.css";

import {useNavigate} from 'react-router-dom';
import {useDispatch} from 'react-redux';
import {resetToken} from 'redux/token.js';
import {resetUser} from 'redux/user.js';


function NavBar(props) {
    const dispatch = useDispatch()
    const navigate = useNavigate();

    function backward() {
        if (props.signOut) {
            dispatch(resetToken());
            dispatch(resetUser());
            navigate("..", {replace: true});
        } else {
            window.location.replace('https://isaev-top.org/dima');
        }
    }


    return (
        <div className="nav-bar">
            <a className="logo" onClick={backward}>
                <img src={require('./logo.png')} alt="logo"/>
                <div className="name-wrapper">{props.headerText}</div>
            </a>
            <div className="nav-wrapper">
                <a href="https://gitlab.com/buharovdp/itmo">Project on GitLab</a>
                <a href="https://t.me/DmitryBuharov">Telegram</a>
                <a href="https://isaev-top.org/">Main page</a>
            </div>
        </div>
    );
}

export default NavBar;