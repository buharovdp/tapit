import "./index.scoped.css";


function CenterWrapper(props) {

    return (
        <div className="center">
            {props.children}
        </div>
    );
}

export default CenterWrapper;