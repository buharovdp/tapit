import React from 'react';
import ReactDOM from 'react-dom/client';
import {createBrowserRouter, RouterProvider} from "react-router-dom";

import store from 'store.js'
import { Provider } from 'react-redux'

import './index.css';

import Sign from 'routes/Sign';
import Main from 'routes/Main';
// import Error from 'routes/Error';
import NotFound from 'routes/NotFound';

const router = createBrowserRouter(
    [
        {
            path: "/",
            element: <Sign/>,
            errorElement: <NotFound/>,
        },
        {
            path: "/app",
            element: <Main/>,
            errorElement: <NotFound/>,
        },
        {
            path: "/*",
            element: <NotFound/>,
        }
    ],
    {
        basename: process.env.PUBLIC_URL
    });
const root = ReactDOM.createRoot(document.getElementById('root'));
root.render(
    <React.StrictMode>
        <Provider store={store}>
            <RouterProvider router={router}/>
        </Provider>
    </React.StrictMode>
);

