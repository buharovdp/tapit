import { createSlice } from '@reduxjs/toolkit'

export const tokenSlice = createSlice({
    name: 'token',
    initialState: {
        value: window.localStorage.getItem("JWT_TOKEN_SLOVENLYPOLYGON"),
    },
    reducers: {
        setToken: (state, action) => {
            window.localStorage.setItem("JWT_TOKEN_SLOVENLYPOLYGON", 'Bearer ' + action.payload);
            state.value = 'Bearer ' + action.payload;
        },
        resetToken: (state) => {
            window.localStorage.removeItem("JWT_TOKEN_SLOVENLYPOLYGON");
            state.value = null;
        },
    },
})

//selector function
export const selectToken = state => state.token.value;

// Action creators are generated for each case reducer function
export const {setToken, resetToken } = tokenSlice.actions;

export default tokenSlice.reducer;