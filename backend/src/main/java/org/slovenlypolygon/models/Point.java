package org.slovenlypolygon.models;

import javax.persistence.*;

import java.sql.Date;
import java.sql.Timestamp;
import java.time.ZonedDateTime;

// JPA
@Entity
@Table(name = "points")
public class Point {
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private long id;
    @Column(name = "x")
    private int x;
    @Column(name = "y")
    private int y;
    @Column(name = "r")
    private int r;
    @Column(name = "date")
    private long requestTimestamp;
    @Column(name = "result")
    private String result;
    @Column(name = "owner")
    private String owner;

    public Point() {
    }

    public Point(int x, int y, int r, String owner) {
        this.x = x;
        this.y = y;
        this.r = r;
        this.requestTimestamp = new Timestamp(System.currentTimeMillis()).getTime();
        this.result = setResult(x, y, r);
        this.owner = owner;
    }

    private String setResult(int x, int y, int r) {
        if (y >= 0 && x <= 0 && y <= x + r / 2.0) return "yes";
        if (y <= 0 && x <= 0 && y >= -r / 2.0 && x >= -r) return "yes";
        if (y <= 0 && x >= 0 && x * x + y * y <= (r/2.0) * (r/2.0)) return "yes";
        return "no";
    }

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public int getX() {
        return x;
    }

    public void setX(int x) {
        this.x = x;
    }

    public int getY() {
        return y;
    }

    public void setY(int y) {
        this.y = y;
    }

    public int getR() {
        return r;
    }

    public void setR(int r) {
        this.r = r;
    }

    public long getRequestTimestamp() {
        return requestTimestamp;
    }

    public void setRequestTimestamp(long requestTimestamp) {
        this.requestTimestamp = requestTimestamp;
    }

    public String getResult() {
        return result;
    }

    public void setResult(String result) {
        this.result = result;
    }

    public String getOwner() {
        return owner;
    }

    public void setOwner(String owner) {
        this.owner = owner;
    }
}
