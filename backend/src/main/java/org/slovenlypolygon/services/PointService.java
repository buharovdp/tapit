package org.slovenlypolygon.services;

import org.slovenlypolygon.models.Point;
import org.slovenlypolygon.repositories.PointRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;
import java.util.Optional;

@Service
@Transactional(readOnly = true)
public class PointService {
    private final PointRepository pointRepository;

    @Autowired
    public PointService(PointRepository pointRepository) {
        this.pointRepository = pointRepository;
    }

    public List<Point> findAll() {
        return pointRepository.findAll();
    }

    public Point findById(long id) {
        var point = pointRepository.findById(id);
        return point.orElse(null);
    }

    @Transactional
    public void save(Point point) {
        pointRepository.save(point);
    }

    @Transactional
    public void delete(long id) {
        pointRepository.deleteById(id);
    }
}
