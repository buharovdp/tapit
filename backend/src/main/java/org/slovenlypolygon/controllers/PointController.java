package org.slovenlypolygon.controllers;

import org.slovenlypolygon.models.Point;
import org.slovenlypolygon.security.JWTUtil;
import org.slovenlypolygon.services.PointService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.dao.EmptyResultDataAccessException;
import org.springframework.web.bind.annotation.*;

import java.util.Arrays;
import java.util.List;
import java.util.Map;

@RestController
@RequestMapping("/api/points")
public class PointController {
    private final PointService pointService;
    private final JWTUtil jwtUtil;

    @Value("${BEARER}")
    private String JWT_START;

    @Autowired
    public PointController(PointService pointService, JWTUtil jwtUtil) {
        this.pointService = pointService;
        this.jwtUtil = jwtUtil;
    }

    @GetMapping()
    public List<Point> index() {
        return pointService.findAll();
    }

    @GetMapping("/{id}")
    public Point show(@PathVariable("id") long id) {
        return pointService.findById(id);
    }

    @PostMapping()
    public Map<String, String> create(@RequestParam("x") int x, @RequestParam("y") int y, @RequestParam("r") int r, @RequestHeader("Authorization") String authHeader) {
        List<Integer> values = Arrays.asList(-3, -2, -1, 0, 1, 2, 3, 4, 5);
        if (authHeader != null && !authHeader.isBlank() && authHeader.startsWith(JWT_START)) {
            String token = authHeader.substring(JWT_START.length());
            var username = jwtUtil.validateTokenAndRetrieveClaim(token);
            if (values.contains(x) && values.contains(y) && values.contains(r) && r > 0) {
                pointService.save(new Point(x, y, r, username));
                return Map.of("message", "success");
            }
            return Map.of("message", "values is incorrect");
        }
        return Map.of("message", "jwt token is expired");
    }

    @DeleteMapping("/{id}")
    public void delete(@PathVariable("id") long id) {
        try {
            pointService.delete(id);
        } catch (EmptyResultDataAccessException e) {
            System.err.println("Can't delete point, ID is not found");
        }
    }
}
