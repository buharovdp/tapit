package org.slovenlypolygon.controllers;

import org.slovenlypolygon.models.User;
import org.slovenlypolygon.security.JWTUtil;
import org.slovenlypolygon.security.UserDetails;
import org.slovenlypolygon.services.SignUpService;
import org.slovenlypolygon.services.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.EmptyResultDataAccessException;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.authentication.BadCredentialsException;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.web.bind.annotation.*;

import java.util.Map;

@RestController
@RequestMapping("/api/sign")
public class SignController {
    private final UserService userService;
    private final SignUpService signUpService;
    private final JWTUtil jwtUtil;
    private final AuthenticationManager authenticationManager;

    @Autowired
    public SignController(UserService userService, SignUpService signUpService, JWTUtil jwtUtil, AuthenticationManager authenticationManager) {
        this.userService = userService;
        this.signUpService = signUpService;
        this.jwtUtil = jwtUtil;
        this.authenticationManager = authenticationManager;
    }

    @PostMapping("/in")
    public Map<String, String> signIn(@RequestParam("username") String username, @RequestParam("password") String password) {

        var token = new UsernamePasswordAuthenticationToken(username, password);
        try {
            authenticationManager.authenticate(token);
        } catch (BadCredentialsException e) {
            return Map.of("message", "Incorrect login or password");
        }

        var newToken = jwtUtil.generateToken(username);
        return Map.of("jwt-token", newToken);
    }

    @PostMapping("/up")
    public Map<String, String> signUp(@RequestParam("username") String username, @RequestParam("password") String password) {
        try {
            userService.loadUserByUsername(username);
        }
        catch (UsernameNotFoundException e) {
            if (!username.isBlank() && !password.isBlank()) {
                signUpService.register(new User(username, password));

                var token = jwtUtil.generateToken(username);
                return Map.of("jwt-token", token);
            }
            return Map.of("message", "Login and password can not be empty");
        }

        return Map.of("message", "Such a user already exists");
    }

    @DeleteMapping("/{id}")
    public void delete(@PathVariable("id") long id) {
        try {
            userService.delete(id);
        } catch (EmptyResultDataAccessException e) {
            System.err.println("Can't delete point, ID is not found");
        }
    }

    @GetMapping("/info")
    public User showUserInfo() {
        var authentication = SecurityContextHolder.getContext().getAuthentication();
        var userDetails = (UserDetails) authentication.getPrincipal();
        var user = userDetails.getUser();
        System.out.println(user);
        return user;
    }
}
