package org.slovenlypolygon.security;

import com.auth0.jwt.JWT;
import com.auth0.jwt.algorithms.Algorithm;
import com.auth0.jwt.exceptions.JWTVerificationException;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;

import java.time.ZonedDateTime;
import java.util.Date;

@Component
public class JWTUtil {
    @Value("${JWT_SECRET}")
    private String SECRET;
    @Value("${EXPIRED}")
    private int EXPIRED_TIME;

    public String generateToken(String username) {
        Date expirationTime = Date.from(ZonedDateTime.now().plusHours(EXPIRED_TIME).toInstant()); // now TimeStamp + 60 min
        return JWT.create()
                .withSubject("User details")
                .withClaim("username", username)
                .withIssuedAt(new Date())
                .withIssuer(SECRET)
                .withExpiresAt(expirationTime)
                .sign(Algorithm.HMAC256(SECRET));
    }

    public String validateTokenAndRetrieveClaim(String token) throws JWTVerificationException {
        var verifier = JWT.require(Algorithm.HMAC256(SECRET))
                .withSubject("User details")
                .withIssuer(SECRET)
                .build();

        var jwt = verifier.verify(token);
        return jwt.getClaim("username").asString();
    }
}
